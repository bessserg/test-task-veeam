import xml.etree.ElementTree as ET
import shutil


def parseXML(path_to_xml_file):
    """
    Функция-генератор, которая возвращает атрибуты и значения для элементов file в виде словаря
    :param path_to_xml_file: путь до XML файла
    """
    tree = ET.parse(path_to_xml_file)
    root = tree.getroot()
    for child in root:
        yield child.attrib


def copy_file_using_config_file(path_to_xml_file, use_prefix=False):
    """
    Функция осуществляющая копирование файлов в соответствии с конфигурационным файлом.
    :param path_to_xml_file: путь до XML файла
    :param use_prefix: использование префикса при копировании в названии файла
    """
    prefix = 'copy_'
    gen = parseXML(path_to_xml_file)
    for item in gen:
        source_path = item['source_path'] + '/' + item['file_name']
        if use_prefix:
            destination_path = item['destination_path'] + '/' + prefix + item['file_name']
        else:
            destination_path = item['destination_path'] + '/' + item['file_name']
        try:
            shutil.copyfile(source_path, destination_path)
        except shutil.SameFileError:
            print('Source and destination represents the same file')
        except IsADirectoryError:
            print('Destination is a directory')
        except PermissionError:
            print('Permission denied')
        except FileNotFoundError as error:
            print(error)
        else:
            print(f'Copy {item["file_name"]} completed successfully')
        

path_to_xml_file = 'path/to/xml/'
copy_file_using_config_file(path_to_xml_file, use_prefix=True)